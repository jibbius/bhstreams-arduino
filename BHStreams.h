/*
**
** This file is part of BHStreams.
**
** Author(s):
**  - Ian Firns <firnsy@gmail.com>
**
** Required Dependencies:
**  - MQTT (by Joel Gehwiler - v2.4.1 or later)
**
** Optional Dependencies:
**  - ArduinoOTA (by Ivan Grokhotkov/Miguel Angel Ajo - v1.0)
**
** BHStreams is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** BHStreams is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with BHStreams.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#ifndef _BH_STREAMS_
#define _BH_STREAMS_

#include <MQTT.h>

#if defined(ESP8266)
#  include <ESP8266WiFi.h>
#elif defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
#  include <WiFi.h>
#endif

#if defined(STREAMS_OTA_ENABLE)
#  if defined(ESP8266)
#    include <ESP8266mDNS.h>
#  elif defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
#    include <ESPmDNS.h>
#  endif
#  include <WiFiUdp.h>
#  include <ArduinoOTA.h>
#endif

#if defined(STREAMS_DEBUG_ENABLE)
#  define DEBUG_PRINT(...) Serial.print(__VA_ARGS__)
#  define DEBUG_PRINTF(...) Serial.printf(__VA_ARGS__)
#  define DEBUG_PRINTLN(...) Serial.println(__VA_ARGS__)
#else
#  define DEBUG_PRINT(...)
#  define DEBUG_PRINTF(...)
#  define DEBUG_PRINTLN(...)
#endif

#define STREAMS_VERSION       "0.2.0"

#define STREAMS_DATA_LENGTH   64
#define STREAMS_TOPIC_LENGTH  256
#define STREAMS_VALUE_LENGTH  32
#define STREAMS_CALLBACKS_MAX 32

#define STREAMS_STATE_NOT_CONNECTED  0
#define STREAMS_STATE_CONNECTING     1
#define STREAMS_STATE_WIFI_CONNECTED 2
#define STREAMS_STATE_WIFI_WITH_IP   3
#define STREAMS_STATE_WIFI_WITH_MQTT 4

class BHStreams;
class BHStreams_Data;

typedef void (*BHStreamsCallback)(BHStreams_Data *data);

typedef struct {
  char *streams[STREAMS_CALLBACKS_MAX] = {NULL};
  BHStreamsCallback cbs[STREAMS_CALLBACKS_MAX] = {NULL};
} BHStreamsCallbackStack;

static BHStreamsCallbackStack callback;

class BHStreams_Data {
  public:
    BHStreams_Data(char bytes[], int length) {
      this->_data_len = length;

      // copy bytes internally
      memset(this->_data, 0, STREAMS_DATA_LENGTH);
      strncpy(this->_data, bytes, length);
    }

    ~BHStreams_Data(void) {
    }

    int dataLength(void) {
      return this->_data_len;
    }

    // type conversion methods

    /**
     * Returns a boolean representation of the object's data.
     *
     * @return Bool.
     */
    bool toBool(void) {
      if (this->_data == NULL) {
        return false;
      }

      return (toInt() > 0 || toDouble() > 0 || this->_data[0] == 't' || this->_data[0] == 'T');
    }

    /**
     * Returns a char array representation of the object's data.
     *
     * @return Char pointer.
     */
    char* toChar(void) {
      return this->_data;
    }

    /**
     * Returns a double representation of the object's data.
     *
     * @return Double.
     */
    double toDouble(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return strtod(this->_data, NULL);
    }

    /**
     * Returns a float representation of the object's data.
     *
     * @return Float.
     */
    float toFloat(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return (float)strtod(this->_data, NULL);
    }

    /**
     * Returns an integer representation of the object's data.
     *
     * @return Integer.
     */
    int toInt(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return (int)strtol(this->_data, NULL, 10);
    }

    /**
     * Returns a long representation of the object's data.
     *
     * @return Long.
     */
    long toLong(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return strtol(this->_data, NULL, 10);
    }

    /**
     * Returns a 32bit RGB encoded representation of the object's data.
     *
     * @return 32-bit unsigned integer.
     */
    uint32_t toNeoPixel(void) {
      if (this->_data == NULL || this->_data_len != 6) {
        return 0;
      }

      char rgb[9] = "0x000000";
      strncpy(&rgb[2], toChar() + 1, 6);
      return (uint32_t)strtol(rgb, NULL, 0);
    }

    /**
     * Returns an Arduino compatible pin level representation of the object's data.
     *
     * @return HIGH if the data represents a true value, LOW otherwise.
     */
    int toPinLevel(void) {
      return toBool() ? HIGH : LOW;
    }

    /**
     * Returns a string object representation of the object's data.
     *
     * @return String.
     */
    String toString(void) {
      if (this->_data == NULL) {
        return String();
      }

      return String(this->_data);
    }

    /**
     * Returns an unsigned integer representation of the object's data.
     *
     * @return Unsigned integer.
     */
    unsigned int toUnsignedInt(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return (unsigned int)strtoul(this->_data, NULL, 10);
    }

    /**
     * Returns an unsigned long representation of the object's data.
     *
     * @return Unsigned long.
     */
    unsigned long toUnsignedLong(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return strtoul(this->_data, NULL, 10);
    }

  private:
    char _data[STREAMS_DATA_LENGTH];
    int _data_len = 0;
};

static void _handle(MQTTClient *client, char topic[], char bytes[], int length) {
  for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
    if (callback.streams[i] != NULL && strncmp(callback.streams[i], topic, strlen(topic)) == 0) {
      BHStreams_Data data(bytes, length);
      callback.cbs[i](&data);
      break;
    }
  }
}

class BHStreams {
  public:
    BHStreams(void) {
    // generate unique client ID (MQTT)
#if defined(ESP8266)
      uint32_t chipId = ESP.getChipId();
      snprintf(this->_clientId, 32, "ESP8266-00%04X", chipId);
#elif defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
      uint64_t chipId = ESP.getEfuseMac();
      snprintf(this->_clientId, 32, "ESP32-%04X%08X", (uint16_t)(chipId>>32), chipId);
#endif

#if defined(STREAMS_OTA_ENABLE)
      ArduinoOTA.setHostname(this->_clientId);
#endif
    };

    ~BHStreams(void) {
      // cleanup allocations
      if (this->_net != NULL) {
        free(this->_net);
      }
    }

    /**
     * Initialise the Streams object and it's subsystems including,
     * WiFi, OTA (if enabled) and MQTT.
     *
     * @param ssid SSID of WiFi network to connect to.
     * @param pass password used to authenticate with WiFi network.
     *
     */
    void begin(const char ssid[], const char pass[]) {
      beginWiFi(ssid, pass);
      beginOTA();
      beginMQTT();
    }

    /**
     * Initialise the MQTT subsystem of the Streams object only.
     *
     */
    void beginMQTT(void) {
      this->_net = new WiFiClient();
      this->_mqtt.begin("ballarathackerspace.org.au", *this->_net);
      this->_mqtt.onMessageAdvanced(_handle);
    }

    /**
     * Initialise the OTA subsystem (if enabled) of the Streams object only.
     *
     */
    void beginOTA(void) {
#if defined(STREAMS_OTA_ENABLE)
      DEBUG_PRINTF("OTA enabled.\n");

      ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
          type = "sketch";
        } else { // U_SPIFFS
          type = "filesystem";
        }

        // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        Serial.printf("Initiating OTA update: %s (%s)\n", type, this->_clientId);
      });

      ArduinoOTA.onEnd([]() {
        Serial.println("\nComplete. Restarting ...");

        delay(500);

        ESP.restart();
      });

      ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
      });

      ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
          Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
          Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
          Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
          Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
          Serial.println("End Failed");
        }
      });

      ArduinoOTA.begin();
#else
      Serial.println("OTA is not enabled.");
#endif
    }

    /**
     * Initialise the WiFi subsystem of the Streams object only.
     *
     * @param ssid SSID of WiFi network to connect to.
     * @param pass password used to authenticate with WiFi network.
     *
     */
    void beginWiFi(const char ssid[], const char pass[]) {
      snprintf(this->_ssid, 64, "%s", ssid);
      snprintf(this->_password, 64, "%s", pass);

      DEBUG_PRINTF("Configured WiFi connection: %s => %s\n", ssid, pass);
    }

    /**
     * Connect to the Streams service.
     *
     */
    void connect(void) {
      this->_wantConnect = true;

      if (!this->_nonBlocking) {
        while (this->_state != STREAMS_STATE_WIFI_WITH_MQTT) {
          this->_process();
          delay(100);
        };
      }
    }

    /**
     * Disconnect gracefully from the Streams service and the WiFi network.
     *
     */
    void disconnect(void) {
      this->_wantConnect = false;

      // disconnect from MQTT if connected
      if (this->_state == STREAMS_STATE_WIFI_WITH_MQTT) {
        DEBUG_PRINTLN("Disconnecting from MQTT ...");
        this->_mqtt.disconnect();
      }

      // disconnect from WiFi if connected
      if (this->_state >= STREAMS_STATE_CONNECTING) {
        _disconnectWiFi();
      }
    }

    char* getClientId(void) {
      return this->_clientId;
    }

    bool isConnected(void) {
      return (this->_state == STREAMS_STATE_WIFI_WITH_MQTT) && this->_mqtt.connected();
    }

    /**
     * DEPRECATED: See onStream.
     *
     */
    void onFeed(const char stream[], BHStreamsCallback cb) {
      Serial.println("DEPRECATED: use onStream(), this will be removed in the next release.");
      onStream(stream, cb);
    }

    /**
     * Register a callback to a specified Streams feed.
     *
     * A Streams feed, underneath, is mereley a subordinate MQTT topic with
     * "streams/" as the root topic. For example a Streams feed of
     * "temperature/kitchen" is equivalent to the MQTT topic
     * "streams/temperature/kitchen".
     *
     * @param topic Streams feed to register callback against.
     * @param cb Function callback to be envoked when data received.
     *
     */
    void onStream(const char stream[], BHStreamsCallback cb) {
      int found = -1;
      char topicSanitised[STREAMS_TOPIC_LENGTH];

      snprintf(topicSanitised, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      DEBUG_PRINTF("Registering callback for stream feed: %s (%s)\n", stream, topicSanitised);

      for (int i=0; i<STREAMS_CALLBACKS_MAX && found==-1; i++) {
        if (callback.cbs[i] != NULL && strncmp(callback.streams[i], topicSanitised, strlen(topicSanitised)) == 0) {
          found = i;
        }
      }

      if (found == -1) {
       for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
          if (callback.cbs[i] == NULL) {
            callback.cbs[i] = cb;

            char *s = new char[strlen(topicSanitised)+1];
            strcpy(s, topicSanitised);
            callback.streams[i] = s;

            this->_link(topicSanitised);
            break;
          }
        }
      } else {
        callback.cbs[found] = cb;
      }
    }

    /**
     * Register a callback to a specified MQTT topic.
     *
     * @param topic Raw MQTT topic to register callback against.
     * @param cb Function callback to be envoked when data received.
     *
     */
    void onTopic(const char topic[], BHStreamsCallback cb) {
      int found = -1;
      char topicSanitised[STREAMS_TOPIC_LENGTH];

      snprintf(topicSanitised, STREAMS_TOPIC_LENGTH, "%s", topic);
      DEBUG_PRINTF("Registering callback to topic feed: %s (%s)\n", topic, topicSanitised);

      for (int i=0; i<STREAMS_CALLBACKS_MAX && found==-1; i++) {
        if (callback.cbs[i] != NULL && strncmp(callback.streams[i], topicSanitised, strlen(topicSanitised)) == 0) {
          found = i;
        }
      }

      if (found == -1) {
       for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
          if (callback.cbs[i] == NULL) {
            callback.cbs[i] = cb;

            char *s = new char[strlen(topicSanitised)+1];
            strcpy(s, topicSanitised);
            callback.streams[i] = s;

            this->_link(topicSanitised);
            break;
          }
        }
      } else {
        callback.cbs[found] = cb;
      }
    }

    /**
     * Process any waiting events and messages on all the Streams subsystems
     * including WiFi, OTA (if enabled) and MQTT.
     *
     */
    void process(void) {
      this->_process();

      if (!this->_nonBlocking) {
        while (this->_state != STREAMS_STATE_WIFI_WITH_MQTT) {
          delay(100);
          this->_process();
        }
      }
    }

    void setClientId(const char id[]) {
      snprintf(this->_clientId, 32, "%s", id);

#if defined(STREAMS_OTA_ENABLE)
      ArduinoOTA.setHostname(this->_clientId);
#endif
    }

    void setConnectedCallback(std::function<void()> func) {
      this->_cbConnected = func;
    }

    void setMQTTHost(const char hostname[]) {
      this->setMQTTHost(hostname, 1883);
    }

    void setMQTTHost(const char hostname[], int port) {
      this->_mqtt.setHost(hostname, port);
    }

    /**
     * Set the non-blocking operating state of the Streams object.
     *
     * If non-blocking mode is disabled, any calls to connect() or
     * process() will block until a WiFi connection has been established
     * and successful registration with the MQTT server.
     *
     * If non-blocking mode is enabled, any calls to connect() or
     * process() will pass through and return control back to the main loop
     * despite a complete connection to the MQTT server being in place.
     *
     * Non-blocking mode is essential for projects where control must be
     * yielded back to the main loop.
     *
     * @param state Nonblocking operating state to be set.
     *
     */
    void setNonBlocking(bool state) {
      this->_nonBlocking = state;
    }

#if defined(STREAMS_OTA_ENABLE)
    // md5 hash of the password
    void setOTAPasswordHash(const char hash[]) {
      ArduinoOTA.setPasswordHash(hash);
    }

    // default: 8266
    void setOTAPort(int port) {
      ArduinoOTA.setPort(port);
    }
#endif

    void setWiFiPassword(const char pass[]) {
      snprintf(this->_password, 64, "%s", pass);
    }

    void setWiFiSSID(const char ssid[]) {
      snprintf(this->_ssid, 64, "%s", ssid);
    }

    /**
     * Write a Streams data object's contents to the specified Stream.
     *
     * @param data Data object to be written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], BHStreams_Data data) {
      return this->_writeStream(stream, data.toChar());
    }

    /**
     * Write a formatted boolean value to the specified Stream. A true value
     * is written as "T" and a false is written as "F".
     *
     * @param data Boolean data to be formatted and written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], bool data) {
      return this->_writeStream(stream, data ? "T" : "F");
    }

    /**
     * Write a char string pointer to the specified Stream.
     *
     * @param data Data to be written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], const char data[]) {
      return this->_writeStream(stream, data);
    }

    /**
     * Write a formatted double value to the specified Stream.
     *
     * @param data Double data to be formatted and written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], double data) {
      char value[STREAMS_VALUE_LENGTH];
      snprintf(value, STREAMS_VALUE_LENGTH, "%f", data);
      return this->_writeStream(stream, value);
    }

    /**
     * Write a formatted float value to the specified Stream.
     *
     * @param data Float data to be formatted and written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], float data) {
      char value[STREAMS_VALUE_LENGTH];
      snprintf(value, STREAMS_VALUE_LENGTH, "%f", data);
      return this->_writeStream(stream, value);
    }

    /**
     * Write a formatted int value to the specified Stream.
     *
     * @param data Int data to be formatted and written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], int data) {
      char value[STREAMS_VALUE_LENGTH];
      snprintf(value, STREAMS_VALUE_LENGTH, "%d", data);
      return this->_writeStream(stream, value);
    }

    /**
     * Write a formatted long value to the specified Stream.
     *
     * @param data Long data to be formatted and written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], long data) {
      char value[STREAMS_VALUE_LENGTH];
      snprintf(value, STREAMS_VALUE_LENGTH, "%l", data);
      return this->_mqtt.publish(stream, value);
    }

    /**
     * Write a String object's data to the specified Stream.
     *
     * @param data String data to be written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], String data) {
      char value[STREAMS_VALUE_LENGTH];
      data.toCharArray(value, data.length());
      return this->_writeStream(stream, value);
    }

    /**
     * Write a formatted unsigned int value to the specified Stream.
     *
     * @param data Unsigned int data to be formatted and written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], unsigned int data) {
      char value[STREAMS_VALUE_LENGTH];
      snprintf(value, STREAMS_VALUE_LENGTH, "%u", data);
      return this->_writeStream(stream, value);
    }

    /**
     * Write a formatted unsigned long value to the specified Stream.
     *
     * @param data Unsigned data to be formatted and written.
     * @returns Boolean true if data was sent, otherwise false.
     *
     */
    bool write(const char stream[], unsigned long data) {
      char value[STREAMS_VALUE_LENGTH];
      snprintf(value, STREAMS_VALUE_LENGTH, "%ul", data);
      return this->_writeStream(stream, value);
    }

  private:
    Client *_net;
    MQTTClient  _mqtt;

    bool _hasConnected = false;
    bool _nonBlocking = false;
    bool _wantConnect;
    char _clientId[32];
    char _password[65];
    char _ssid[65];
    uint8_t _state = 0;
    uint32_t _lastMQTTConnect = 0;
    uint32_t _lastWiFiConnect = 0;
    uint32_t _connectAttempts = UINT_MAX;
    uint32_t _connectInterval = 1000;

    // callbacks
    std::function<void()> _cbConnected;

    void _connectWiFi(void) {
      if (this->_wantConnect) {
        this->_state = STREAMS_STATE_CONNECTING;
        WiFi.begin(this->_ssid, this->_password);
        Serial.printf("Connecting to WiFi (%s) ... \n", this->_clientId);
      }
    }

    void _disconnectWiFi(void) {
      this->_connectAttempts = 0;

      // clear ESP's last SSID/PASS cache
      WiFi.disconnect();

      DEBUG_PRINTF("Disconnecting from WiFi ... \n");
    }

    bool _hasLocalIP(void) {
#if defined(ESP8266)
      return WiFi.localIP().isSet();
#elif defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
      return WiFi.localIP() != INADDR_NONE;
#endif
      return false;
    }

    void _link(const char stream[]) {
      if (this->isConnected()) {
        DEBUG_PRINTF("Subscribing to stream: %s\n", stream);
        this->_mqtt.subscribe(stream);
      }
    }

    void _process(void) {
      uint32_t now = millis();

      if (this->_state == STREAMS_STATE_WIFI_WITH_MQTT) {
        // process MQTT and check for return on loop process, it returns
        // false if not connected or an error occurred and the connection
        // was closed.
        if (!this->_mqtt.loop()) {
          this->_state = STREAMS_STATE_WIFI_WITH_IP;
          this->_lastMQTTConnect = now;
        };
      } else if (this->_state == STREAMS_STATE_WIFI_WITH_IP) {
        if (!_hasLocalIP()) {
          DEBUG_PRINTLN("Lost IP address");

          this->_state = STREAMS_STATE_WIFI_CONNECTED;
        } else if (this->_mqtt.connected()) {
          // handle MQTT initial connection if we've just connected
          Serial.println("Connected to MQTT/Streams");

          this->_state = STREAMS_STATE_WIFI_WITH_MQTT;

          // re-link any feeds
          for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
            if (callback.streams[i] != NULL) {
              this->_link(callback.streams[i]);
            }
          }

          if (this->_cbConnected != NULL) {
            this->_cbConnected();
          }
        } else if (now - this->_lastMQTTConnect > 2000) {
          // otherwise attempt to connect
          Serial.printf("Connecting to MQTT/Streams (%s) ... \n", this->_clientId);
          this->_mqtt.connect(this->_clientId);
          this->_lastMQTTConnect = now;
        }
      } else if (this->_state == STREAMS_STATE_WIFI_CONNECTED) {
        if (_hasLocalIP()) {
          DEBUG_PRINT("Obtained IP address: ");
          DEBUG_PRINTLN(WiFi.localIP());

          this->_state = STREAMS_STATE_WIFI_WITH_IP;
        } else if (WiFi.status() != WL_CONNECTED) {
          Serial.println("Disconnected from WiFi");

          this->_state = STREAMS_STATE_NOT_CONNECTED;
          _connectWiFi();
        }
      } else if (this->_state == STREAMS_STATE_CONNECTING) {
        if (WiFi.status() == WL_CONNECTED) {
          DEBUG_PRINTLN("Connected to WiFi");

          this->_state = STREAMS_STATE_WIFI_CONNECTED;
          this->_hasConnected = true;
          this->_connectAttempts = 0;
        } else if (this->_connectAttempts > 20) {
          DEBUG_PRINTF("Connection timeout!\n");
          this->_state = STREAMS_STATE_NOT_CONNECTED;
        } else if (now - this->_lastWiFiConnect >= this->_connectInterval) {
          DEBUG_PRINT(".");
          this->_lastWiFiConnect = now;
          this->_connectAttempts++;
        }
      } else if (this->_state == STREAMS_STATE_NOT_CONNECTED) {
        if (this->_wantConnect && WiFi.status() != WL_CONNECTED) {
          _disconnectWiFi();
          _connectWiFi();
        }
      }

#if defined(STREAMS_OTA_ENABLE)
      // process OTA if we at least have an IP
      if (this->_state >= STREAMS_STATE_WIFI_WITH_IP) {
        // process OTA
        ArduinoOTA.handle();
      }
#endif
    }

    bool _writeStream(const char stream[], const char data[]) {
      if (!this->isConnected()) {
        return false;
      }

      char topic[STREAMS_TOPIC_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      return this->_mqtt.publish(topic, data, (int)strlen(data));
    }
};

#endif
